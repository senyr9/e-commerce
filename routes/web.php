<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController ;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController ;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home');

Route::get('/home/{firstname}/{lastname}', function ($firstname, $lastname){

    return view('home', compact('firstname', 'lastname')) ;
});

Route::resource('categories', CategoryController::class) ;

Auth::routes();

Route::get("logout", [HomeController::class, 'logout']) ;

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth']],
    function(){
    Route::resource('roles', RoleController::class) ;
    Route::resource('users', UserController::class) ;
});
