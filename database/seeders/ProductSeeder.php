<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=10; $i++){
            if ($i <4)
            {
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000*$i,
                    'category_id' => 1,
                    'description' => "lorem lorem epson lorem lorem lorem epson lorem",
                ]);
            }elseif ($i>=4 && $i< 8){
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000*$i,
                    'category_id' => 2,
                    'description' => "lorem lorem epson lorem lorem lorem epson lorem",
                ]);
            }else{
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000*$i,
                    'category_id' => 3,
                    'description' => "lorem lorem epson lorem lorem lorem epson lorem",
                ]);
            }
        }
    }
}
