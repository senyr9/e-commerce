@extends('layouts.layout')

@section("title") {{ $user->name }} @endsection

@section('body')
    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                <div class="pull-left">
                    <h2>Affichage d'un utilisateur</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <strong>Nom complet :</strong>
                    {{ $user->name }}
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong>Email :</strong>
                    {{ $user->email }}
                </div>
            </div>


            <div class="col-12">
                <div class="form-group">
                    <strong>Roles :</strong>
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $role)
                            <label class="badge badge-success">
                                {{ $role }}
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
