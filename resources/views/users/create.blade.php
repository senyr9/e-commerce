@extends('layouts.layout')

@section("title") Enregister un nouvel utilisateur @endsection

@section('body')
    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'users.store', 'method' => 'POST')) !!}
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Nom complet</strong>
                            {!! Form::text('name', null,
                            array('placeholder' => 'Nom de l\'utilisateur', 'class' =>'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Email</strong>
                            {!! Form::email('email', null,
                            array('placeholder' => 'Email', 'class' =>'form-control')) !!}
                        </div>
                    </div>
                <div class="col-12">
                        <div class="form-group">
                            <strong>Mot de passe</strong><br>
                            {!! Form::password('password', null,
                            array('placeholder' => 'Mot de passe', 'class' =>'form-control')) !!}
                        </div>
                </div>
                <div class="col-12">
                        <div class="form-group">
                            <strong>Confirmation du mot de passe</strong><br>
                            {!! Form::password('confirm-password', null,
                            array('placeholder' => 'répéter le mot de passe', 'class' =>'form-control')) !!}
                        </div>
                </div>
                <div class="col-12">
                        <div class="form-group">
                            <strong>Rôles</strong><br>
                            {!! Form::select('roles[]', $roles, [],
                            array('class' => 'form-control', 'multiple')) !!}
                        </div>
                    </div>
                <div class="col-12">
                        <button type="submit" class="btn btn-block btn-primary">
                            Enregistrer
                        </button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
