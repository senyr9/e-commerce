@extends('layouts.app')

@section('content')

    <h3>Formulaire d'ajout d'une nouvelle catégorie</h3>

    <form action="{{ route('categories.store') }}" method="post">
        @csrf

        @include('categories.form')

    </form>

@endsection
