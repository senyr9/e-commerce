@extends('layouts.app')

@section('slideshow') @endsection
@section('banner') @endsection

@section('content')

    <section>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4">

                        <ul class="list-group">
                            <li class="list-group-item d-flex
                            justify-content-md-end align-items-center">
                                <a href="{{ route('users.index') }}">
                                    Gestion des utilisateurs
                                    <span class="badge badge-primary badge-pill">
                                    10
                                </span>
                                </a>
                            </li>
                            <li class="list-group-item d-flex
                            justify-content-md-end align-items-center">
                                Gestion des rôles
                                <span class="badge badge-primary badge-pill">
                                    1
                                </span>
                            </li>
                        </ul>

                    </div>
                    <div class="col-8">
                        @yield('body')
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
